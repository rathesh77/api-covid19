package fr.covidapi.project;

public class FranceData {
    private String date;
    private String nom;
    private int hospitalises;
    private int reanimation;
    private int nouvellesHospitalisations;
    private int nouvellesReanimations;
    private int deces;
    private int gueris;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getHospitalises() {
        return hospitalises;
    }

    public void setHospitalises(int hospitalises) {
        this.hospitalises = hospitalises;
    }

    public int getReanimation() {
        return reanimation;
    }

    public void setReanimation(int reanimation) {
        this.reanimation = reanimation;
    }

    public int getNouvellesHospitalisations() {
        return nouvellesHospitalisations;
    }

    public void setNouvellesHospitalisations(int nouvellesHospitalisations) {
        this.nouvellesHospitalisations = nouvellesHospitalisations;
    }

    public int getNouvellesReanimations() {
        return nouvellesReanimations;
    }

    public void setNouvellesReanimations(int nouvellesReanimations) {
        this.nouvellesReanimations = nouvellesReanimations;
    }

    public int getDeces() {
        return deces;
    }

    public void setDeces(int deces) {
        this.deces = deces;
    }

    public int getGueris() {
        return gueris;
    }

    public void setGueris(int gueris) {
        this.gueris = gueris;
    }

}
