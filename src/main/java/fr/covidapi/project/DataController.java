package fr.covidapi.project;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class DataController {

    @Autowired
    CovidDAO covidDAO;

    @GetMapping(value = "/")
    public String index() {
        return "Hello World !";
    }

    @GetMapping(value = "/cases/global")
    public List<FranceData> getGlobalData() {
        return covidDAO.getGlobalData();
    }

    @GetMapping(value = "/cases/departements")
    public List<FranceData> getGlobalDataForEveryDepartement() {
        return covidDAO.getGlobalDataForEveryDepartement();
    }

    @GetMapping(value = "/cases/{departement}")
    public List<FranceData> getGlobalDataForDepartement(@PathVariable String departement) {
        return covidDAO.getGlobalDataForDepartement(departement);
    }

    @GetMapping(value = "/cases/{date}/{departement}")
    public FranceData getDataForOneDepartementFromDate(@PathVariable String date, @PathVariable String departement) {
        return covidDAO.getDataForOneDepartementFromDate(date, departement);
    }

    @GetMapping(value = "/cases/delta/{date}/{departement}")
    public FranceData getDeltaOfGlobalDataFromDateToYesterdayForDepartement(@PathVariable String date, @PathVariable String departement) {
        return covidDAO.getDeltaOfGlobalDataFromDateToYesterdayForDepartement(date, departement);
    }


}
