package fr.covidapi.project;

import java.util.List;

public interface CovidDAO {
    /**
     * affiche les données globales de toute la France
     *
     * @return List<FranceData>
     */
    List<FranceData> getGlobalData();

    /**
     * affiche les données globales tout departement confondu
     *
     * @return List<FranceData>
     */
    List<FranceData> getGlobalDataForEveryDepartement();

    /**
     * affiche les données globales pour un departement
     *
     * @param departement
     * @return List<FranceData>
     */
    List<FranceData> getGlobalDataForDepartement(String departement);

    /**
     * affiche les données du departement donné à la date donnée
     *
     * @return List<FranceData>
     */
    FranceData getDataForOneDepartementFromDate(String date, String departement);

    /**
     * affiche la difference des données globales entre les données à la date T et les données d'aujourd'hui
     *
     * @param departement
     * @return List<FranceData>
     */
    FranceData getDeltaOfGlobalDataFromDateToYesterdayForDepartement(String date, String departement);
}
