package fr.covidapi.project;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
@Primary
public class CovidDAOFichier implements CovidDAO {
    private static final String BASE_URL = "https://coronavirusapi-france.now.sh";
    private OkHttpClient client = new OkHttpClient();
    private File file = new File("global.json");
    private ObjectMapper objectMapper = new ObjectMapper();

    CovidDAOFichier() {
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    List<FranceData> requestApi(String endpoint, String property) {
        List<FranceData> data = null;
        try {
            var request = new Request.Builder()
                    .url(BASE_URL + endpoint)
                    .build();

            var response = client.newCall(request).execute();
            var responseString = response.body().string();
            var jsonNode = objectMapper.readTree(responseString).get(property);
            var franceGlobalLiveData = objectMapper.writeValueAsString(jsonNode);
            data = objectMapper.readValue(franceGlobalLiveData, new TypeReference<List<FranceData>>() {
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }

    @Override
    public List<FranceData> getGlobalData() {
        List<FranceData> data = null;
        var dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        var now = LocalDateTime.now();

        try {
            data = objectMapper.readValue(file, new TypeReference<List<FranceData>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (data != null && data.get(0) != null && data.get(0).getDate() != null && data.get(0).getDate().equals(dtf.format(now))) {
            return data;
        }
        data = requestApi("/FranceLiveGlobalData", "FranceGlobalLiveData");
        try {
            objectMapper.writeValue(file, data);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return data;
    }

    @Override
    public List<FranceData> getGlobalDataForEveryDepartement() {
        return requestApi("/AllLiveData", "allLiveFranceData");
    }

    @Override
    public List<FranceData> getGlobalDataForDepartement(String departement) {
        return requestApi("/LiveDataByDepartement?Departement=" + departement, "LiveDataByDepartement");
    }

    @Override
    public FranceData getDataForOneDepartementFromDate(String date, String departement) {
        List<FranceData> dataFROM = requestApi("/AllDataByDate?date=" + date, "allFranceDataByDate");
        for (var i = 0; i < dataFROM.size(); i++) {
            if (dataFROM.get(i).getNom().equals(departement)) {
                return dataFROM.get(i);
            }
        }
        return new FranceData();

    }

    @Override
    public FranceData getDeltaOfGlobalDataFromDateToYesterdayForDepartement(String date, String departement) {

        FranceData dataFromDate = this.getDataForOneDepartementFromDate(date, departement);
        FranceData dataFromNow = this.getDataForOneDepartementFromDate(getYesterdayDateString(), departement);

        if (dataFromNow == null || dataFromDate == null) {
            return new FranceData();
        }
        var delta = new FranceData();
        delta.setDate(date + " -> " + dataFromNow.getDate());
        delta.setNom(departement);

        delta.setHospitalises(dataFromNow.getHospitalises() - dataFromDate.getHospitalises());
        delta.setReanimation(dataFromNow.getReanimation() - dataFromDate.getReanimation());
        delta.setNouvellesHospitalisations(dataFromNow.getNouvellesHospitalisations() - dataFromDate.getNouvellesHospitalisations());
        delta.setNouvellesReanimations(dataFromNow.getNouvellesReanimations() - dataFromDate.getNouvellesReanimations());
        delta.setDeces(dataFromNow.getDeces() - dataFromDate.getDeces());
        delta.setGueris(dataFromNow.getGueris() - dataFromDate.getGueris());

        return delta;
    }

    private Date yesterday() {
        final var cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        return cal.getTime();
    }

    private String getYesterdayDateString() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormat.format(yesterday());
    }
}
