package fr.covidapi.project;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class ProjectApplicationTests {

    private CovidDAOFichier daoFichier = new CovidDAOFichier();

    @Test
    void contextLoads() {
    }

    // On test si la fonction CovidDAOFichier::getGlobalData() renvoit bien un object de type FranceData avec tous les attributs non-null
    // cela nous permet de savoir si l'API qu'on interroge nous renvoit bien une reponse
    // Ainsi, on a pas besoin de tester les autres fonction (ex: getDeltaOfGlobalDataFromDateToNowForDepartement()...) car elles ont un peu près le même comportement
    // à quelques details
    @Test
    void testGlobalData() {
        Assertions.assertNotSame(daoFichier.getGlobalData().get(0), new FranceData());
    }

}
